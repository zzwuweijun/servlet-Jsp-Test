<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	HttpServletRequest r = request;
	String path = r.getContextPath();
	String basePath = r.getScheme() + "://" + r.getServerName() + ":" + r.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<form action="user">
		<input type="hidden" name="oper" value="reg">
		<table>
			<tr>
				<td>用户名：</td>
				<td><input type="text" name="uname" data-options="required:true" placeholder="必填"></td>
			</tr>
			<tr>
				<td>密码：</td>
				<td><input type="password" name="pwd" placeholder="必填"></td>
			</tr>
			<tr>
				<td>性别：</td>
				<td>男：<input type="radio" name="sex" value="1"
						checked="checked"> 女：<input type="radio" name="sex"
						value="0">
				</td>
			</tr>
			<tr>
				<td>年龄：</td>
				<td><input type="text" name="age"></td>
			</tr>
			<tr>
				<td>生日：</td>
				<td><input type="text" name="birth" placeholder="2019-12-19"></td>
			</tr>
			<tr>
				<td colspan="2"><input type="submit" value="注册"></td>
			</tr>
		</table>
	</form>
	<br>
	<a href="main/index.jsp">返回主页</a>
</body>
</html>