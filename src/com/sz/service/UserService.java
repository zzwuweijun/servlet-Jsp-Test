package com.sz.service;

import java.sql.SQLException;
import java.util.List;

import com.sz.pojo.User;

public interface UserService {
	/**
	 * 用户校验登录	
	 * @param uname 用户名
	 * @param pwd 用户密码
	 * @return 返回查询到的用户信息
	 */
	User checkUserLoginService(String uname, String pwd);

	/**
	 *用户修改密码 
	 * @param newPwd
	 * @param uid
	 * @return
	 */
	int userChangePwdSer(String newPwd, int uid);
	
	/**
	 * 查看所有用户信息
	 * @return
	 * @throws SQLException 
	 */
	List<User> userShowService() ;

	/**
	 * 用户注册
	 * @param u
	 * @return
	 */
	int userRegService(User u);
}
