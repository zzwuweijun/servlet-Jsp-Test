<%@page import="java.util.ArrayList"%>
<%@page import="com.sz.pojo.User"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	HttpServletRequest r = request;
	String path = r.getContextPath();
	String basePath = r.getScheme() + "://" + r.getServerName() + ":" + r.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>

	<table style="border: 1px solid blue;">
		<tr style="border-bottom: 1px solid blue;">
			<th>用户ID</th>
			<th>用户名</th>
			<th>用户密码</th>
			<th>性别</th>
			<th>年龄</th>
			<th>生日</th>
		</tr>
		<%
			List<User> lu = (ArrayList<User>) request.getAttribute("lu");
			for (User u : lu) {
				String sex = "男";
				if (Integer.parseInt(u.getSex()) != 1) {
					sex = "女";
				}
		%>
		<tr>
			<td><%=u.getId()%></td>
			<td><%=u.getUname()%></td>
			<td><%=u.getPwd()%></td>
			<td><%=sex%></td>
			<td><%=u.getAge()%></td>
			<td><%=u.getBirth()%></td>
		</tr>
		<%
			}
		%>
	</table>


	<br>
	<a href="main/index2.jsp">返回主页</a>
</body>
</html>