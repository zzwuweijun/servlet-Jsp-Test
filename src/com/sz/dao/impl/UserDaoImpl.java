package com.sz.dao.impl;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;
import com.sz.dao.UserDao;
import com.sz.pojo.User;

public class UserDaoImpl implements UserDao {
	// 根据用户名和密码查询用户信息
	@Override
	public User checkUserLoginDao(String uname, String pwd) {
		// 声明jdbc对象
		java.sql.Connection con = null;
		PreparedStatement ps = null;
		java.sql.ResultSet rs = null;
		// 声明变量
		User u = null;

		try {
			// // 创建sql命令
			String sql = "select * from user where uname=? and pwd=?";
			rs = MySql.query(sql, uname, pwd);

			// 遍历结果
			while (rs.next()) {
				u = new User();
				u.setId(rs.getInt("id"));
				u.setUname(rs.getString("uname"));
				u.setPwd(rs.getString("pwd"));
				u.setSex(rs.getString("sex"));
				u.setAge(rs.getInt("age"));
				u.setBirth(rs.getString("birth"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		// 返回查询结果
		return u;
	}

	@Override
	public int userChangePwdDao(String newPwd, int uid) {
		String sql = "update user set pwd=? where id=?";

		int insult = MySql.update(sql, newPwd, uid);
		return insult;
	}

	@Override
	public int userRegDao(User u) {
		String sql = "INSERT INTO user(uname, pwd, sex, age, birth) VALUES(?,?,?,?,?)";
		return MySql.update(sql, u.getUname(), u.getPwd(), u.getSex(), u.getAge(), u.getBirth());
	}

	// 查看所有用户信息
	@Override
	public List<User> userShowDao() {
		String sql = "select * from user";
		List<User> lu = null;
		ResultSet rs = MySql.query(sql);
		lu = new ArrayList<User>();
		// 遍历结果
		try {
			while (rs.next()) {
				User u = new User();
				u.setId(rs.getInt("id"));
				u.setUname(rs.getString("uname"));
				u.setPwd(rs.getString("pwd"));
				u.setSex(rs.getString("sex"));
				u.setAge(rs.getInt("age"));
				u.setBirth(rs.getString("birth"));
				lu.add(u);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return lu;
	}

}
