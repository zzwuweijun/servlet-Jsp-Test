package com.sz.dao.impl;

import java.util.List;

import org.junit.Test;

import com.sz.pojo.User;

public class Page {
	private final static Integer PAGESIZE = 10;	//默认每页的最大记录数为10
	private static final Integer PAGENUM = 1;	//默认是第一页
	/**
	 * 每页的最大记录数，默认是每页有十条记录。
	 */
	private Integer pageSize = PAGESIZE;	
	/**
	 * 当前页是第几页，默认是第一页。
	 */
	private Integer pageNum = PAGENUM;
	/**
	 * 查询到的所有记录的总数。
	 */
	private Integer total;
	/**
	 * 查询到的所有记录进过计算后得到的总页数。
	 */
	private Integer totalPages;
	/**
	 * 
	 */
	private Integer startItems;
	/**
	 * 
	 */
	private Integer endItems;
	
	
	@Test
	public void t2() {
		User u = new User();
		List<User> uu =  (List<User>)MySql.queryAutoMany(u, "select * from user limit 0,5 ");
		System.out.println(uu);
	}
	
}
