package com.util;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;

public class DateTest {
	
	@Test
	public void t3() {
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		System.out.println(df.format(new Date()));
		System.out.println(df.toLocalizedPattern());
		System.out.println(df.toPattern());
		System.out.println(df.getCalendar());
		System.out.println(df.getTimeZone());
		SimpleDateFormat df2 = new SimpleDateFormat("yyyy");  
		System.out.println(df2.format(new Date()));
	}

//	@Test
	public void t2() {
		Date date = new Date();
		System.out.println(date);
		System.out.println(date.getTime());
		System.out.println(date.getDay());
		System.out.println(date.getDate());
	}

//	@Test
	public void t1() {
		long time = System.currentTimeMillis();
		System.out.println(time);
	}
}
