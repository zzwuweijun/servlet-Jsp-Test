<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	HttpServletRequest r = request;
	String path = r.getContextPath();
	String basePath = r.getScheme() + "://" + r.getServerName() + ":" + r.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<title>Insert title here</title>
<script type="text/javascript" src="js/jquery-3.4.1.min.js"></script>
</head>
<body>
	<form action="user" id="fm">
		<input type="hidden" name="oper" value="pwd">
		<label>新密码</label><input type="text" name="newPwd" id="newPwd"/><br/>
		<label>确认密码</label><input type="text" name="" id="cfPwd"/><br/>
		<input type="submit" value="修改密码">
	</form>
	<br>
	<a href="main/index2.jsp">返回主页</a>
	
	<script type="text/javascript">
		$("#fm").submit(function(){
			if($("#newPwd").val()==""){
				alert("密码不能为空");
			}else if($("#cfPwd").val()==""){
				alert("确认密码不能为空");
			}else if($("#newPwd").val() != $("#cfPwd").val()){
				alert("两次密码不一致");
			}
		})
	</script>
</body>
</html>