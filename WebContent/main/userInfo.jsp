<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page import="com.sz.pojo.User"%>
<%
	HttpServletRequest r = request;
	String path = r.getContextPath();
	String basePath = r.getScheme() + "://" + r.getServerName() + ":" + r.getServerPort() + path + "/";
%>
<!DOCTYPE html>
<html>
<head>
<base href="<%=basePath%>">
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<table style="border: 1px solid blue;">
		<tr style="border-bottom: 1px solid blue;">
			<th>用户ID</th>
			<th>用户名</th>
			<th>用户密码</th>
			<th>性别</th>
			<th>年龄</th>
			<th>生日</th>
		</tr>
		<tr>
			<td><%=((User)session.getAttribute("user")).getId() %></td>
			<td><%=((User)session.getAttribute("user")).getUname() %></td>
			<td><%=((User)session.getAttribute("user")).getPwd() %></td>
			<%
				User u = ((User)session.getAttribute("user"));
				String sex = "男";
				System.out.print(u.getSex());
				if (Integer.parseInt(u.getSex()) != 1) {
					sex = "女";
				}
			%>
			<td><%=sex %></td>
			<td><%=((User)session.getAttribute("user")).getAge() %></td>
			<td><%=((User)session.getAttribute("user")).getBirth() %></td>
		</tr>
	</table>
	<br>
	<a href="main/index2.jsp">返回主页</a>
</body>
</html>