package com.util;

import org.junit.Test;

public class StringTest {
	@Test
	public void t6() {
		String a = "deodjoefejodovnewob.djfowes.feofjewfs.fewfo";
		System.out.println(a.replace('j', '*'));
		a.replaceAll("/jodov/", "******---ssssss");	
		System.out.println(a.replace("jo", "***"));
		System.out.println(a.replaceAll("j.*b", "***"));
	}

	// @Test
	public void t5() {
		String a = "o22sj342o";
		System.out.println(a.indexOf("2"));
		String b = "www.runoob.com";
		System.out.println(b.matches("(.*)runoob(.*)"));
	}

	// @Test
	public void t4() {
		String a = "o22sj342o";
		System.out.println(a.endsWith("j"));
		System.out.println(a.endsWith("jo"));
		System.out.println(a.equals("osjo"));
		byte[] b = a.getBytes();
		for (int i = 0; i < b.length; i++) {
			System.out.println((char) b[i]);
		}
		System.out.println(a.intern());
		System.out.println(a.indexOf(2));
	}

	// @Test
	public void t3() {
		String a = "abc";
		String b = "abd";
		System.out.println(a.compareTo(b));
		System.out.println(a.concat("ddd"));
		System.out.println(a.contentEquals("abc"));
	}

	// @Test
	public void t2() {
		String s = "abcqwnghijklmnopqrstuvwxyz";
		System.out.println(s.charAt(2));
		System.out.println(s.codePointAt(2));
		System.out.println(s.codePointBefore(2));
		System.out.println(s.codePointCount(2, 3));
	}

	// @Test
	public void t1() {
		String s1 = "djjdjiepdjeo";

		System.out.println(String.format("w23wj", "%d"));

		System.out.println(s1.join("-*-", "s1", "wwj"));

		System.out.println(String.valueOf("L"));

	}
}
