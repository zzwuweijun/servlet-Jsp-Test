package com.sz.dao;

import java.sql.SQLException;
import java.util.List;

import com.sz.pojo.User;

public interface UserDao {
	/**
	 * 根据用户名和密码查询用户信息
	 * @param uname 用户名
	 * @param pwd 密码
	 * @return 返回查询到的用户信息
	 */
	User checkUserLoginDao(String uname, String pwd);
	/**
	 * 更据用户Id修改密码
	 * @param newPwd
	 * @param uid
	 * @return
	 */
	int userChangePwdDao(String newPwd, int uid);
	
	/**
	 * 查看所有用户信息
	 * @return
	 * @throws SQLException 
	 */
	List<User> userShowDao() ;
	/**
	 * 注册用户
	 * @param u
	 * @return
	 */
	int userRegDao(User u);
}
