package com.tool;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import org.junit.Test;

import com.sz.pojo.User;

public class testMySQL {
	public int i;
	public Integer iiz;
	
	
	@Test
	public void t9() { 
		String sql = "select * from user where id=3";
		ResultSet rs = MySQL.query(sql);
		try {
			ResultSetMetaData rsm =  rs.getMetaData();
			System.out.println(rsm.getColumnClassName(2));
			System.out.println(rsm.getCatalogName(2));
			System.out.println(rsm.getColumnLabel(2));
			System.out.println(rsm.getColumnName(2));
			System.out.println(rsm.getColumnType(2));
			System.out.println(rsm.getColumnTypeName(2));
			System.out.println(rsm.getTableName(2));
		} catch (SQLException e1) {
			e1.printStackTrace();
		}
		try {
			while(rs.next()) {
				System.out.println(rs.getObject("uname"));
				System.out.println(rs.getObject("sex"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
//	@Test
	public void t8() { 
		User u = new User();
		String sql = "select * from user";
		List<User> lu = (List<User>)MySQL.queryPojoObjectList(u, sql);
//		System.out.println(lu);
//		System.out.println(lu.get(2));
		for (int i = 0; i < lu.size(); i++) {
			System.out.println(lu.get(i));
		}
	}
	
//	@Test
	public void t7() {
		User u = new User();
		String sql = "select * from user where id=2";
		u = (User) MySQL.queryOnePojoObject(u, sql);
		System.out.println(u);
	}
	
//	@Test
	public void t6() {
		testMySQL t = new testMySQL();
		try {
			Field f = t.getClass().getField("ii");
			System.out.println(f.getType());
			if(f.getType().equals(Integer.class)) {
				System.out.println("ddd");
			}else {
				System.out.println("ccc");
			}
			
		} catch (NoSuchFieldException | SecurityException e) {
			e.printStackTrace();
		}
	}
	
//	@Test
	public void t5() {
		System.out.println("lll");
		String[] str = "java.lang.String".split("\\.");
		System.out.println(str.length);
		System.out.println(String.class.getTypeName());
		for (int i = 0; i < str.length; i++) {
			System.out.println(str.length);
			System.out.println(str[i]);
		}
		String str1 = "HelloWord";
		String[] temp = str1.split("o");
		System.out.println(temp.length);
	}
	
//	@Test
	public void t4() {
		testMySQL t = new testMySQL();
		System.out.println(t.getClass().getTypeName());
		System.out.println(t.getClass().getName());
		Field[] f = t.getClass().getDeclaredFields();
		System.out.println(f[0].getName());
		System.out.println(f[0].getType());
		System.out.println(f[1].getName());
		System.out.println(f[1].getType());
	}
	
	public void t3(String str) {
		System.out.println(str);
	}
	
//	@Test
	public void t2() {
		testMySQL t = new testMySQL();
		try {
			Method[] m = t.getClass().getMethods();
			System.out.println(m[0].getName());
			m[0].invoke(t, "myNamw");
			Method mm = t.getClass().getMethod("t3", String.class);
			mm.invoke(t, "myNamw");
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		}
	}
	
//	@Test
	public void t1() {
		User u = new User();
		ResultSet rs = MySQL.query("select * from user where id=1");
		try {
			while(rs.next()) {
				u.setAge((Integer) null);
				u.setAge(rs.getInt("age"));
				u.setBirth(rs.getString("birth"));
				u.setId(rs.getInt("id"));
//				u.setPwd(null);
				u.setPwd(rs.getString("pwd"));
				u.setSex(rs.getString("sex"));
				u.setUname(rs.getString("uname"));
			}
			System.out.println(u);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
