package com.tool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.sz.pojo.User;

public class testPage {

	@Test
	public void t11() {
		String sql = "select * from user";
		User u = new User();
		Page p = new Page(u, sql);
		System.out.println(p.queryPage(5));
		System.out.println(p.getPageNum());
		System.out.println(p.getTotalPages());
	}
	
//	@Test
	public void t10() throws SQLException {
		String sql = "select * from user";
		User u = new User();
		Page p = new Page(u, sql);
		p.query(2, 5);
		System.out.println(p.getList());
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
		p.next();
		System.out.println(p.getList());
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
	}

//	 @Test
	public void t9() throws SQLException {
		System.out.println(3 / 6);
	}

//	 @Test
	public void t8() throws SQLException {
		String sql = "select * from user";
		User u = new User();
		Page p = new Page(u, sql);
		System.out.println(p.getList());
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getTotalPages());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
	}

//	 @Test
	public void t7() throws SQLException {
		String ceshi = "select * from user limit 0,3";
		String sql = "select * from user";
		int a = 0;
		int b = 2;
		String newsql = sql + " limit " + a + "," + b;
		System.out.println(newsql);
		List<User> lu = MySQL.queryPojoObjectList(new User(), newsql);
		System.out.println(lu);
	}

//	 @Test
	public void t6() throws SQLException {
		String sql = "select * from user";
		String newsql = sql.replace("*", "count(*)");
		ResultSet rs = MySQL.query(newsql);
		while (rs.next()) {
			System.out.println(rs.getObject(1));
		}
	}

//	 @Test
	public void t5() {
		User u = new User();
		Page p = new Page(u, "select * from user");
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getList());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getTotalPages());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
	}

//	 @Test
	public void t4() {
		ResultSet rs = MySQL.query("select * from user");
		User u = new User();
		@SuppressWarnings("unchecked")
		Page<User> p = new Page<User>(u, rs);
		p.query(2, 13);
		
		System.out.println(p.getList());
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
	}

//	 @Test
	public void t3() {
		List<Object> lu = new ArrayList<Object>();
		lu = MySQL.queryPojoObjectList(new User(), "select * from user");
		Page p = new Page(lu);
		p.query(2, 3);
		System.out.println(p.getList());
		System.out.println(p.getPageSize());
		System.out.println(p.getTotalPages());
		System.out.println(p.getPageNum());
		System.out.println(p.getTotal());
		System.out.println(p.getStartItems());
		System.out.println(p.getEndItems());
	}

//	 @Test
	public void t2() {
		int a = 99;
		int b = 32;
		System.out.println(a / b);
		System.out.println(a % b);
	}

//	 @Test
	public void t1() {
		List<Object> lu = new ArrayList<Object>();
		// lu = MySQL.queryPojoObjectList(new User(), "select * from user limit 0,2");
		lu = MySQL.queryPojoObjectList(new User(), "select * from user");
		Page p = new Page(lu);
		System.out.println(p.getList());
		User u = new User();
		u = (User) p.getList().get(1);
		System.out.println(u.getUname());
	}
}
