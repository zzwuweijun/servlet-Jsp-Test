package com.sz.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.apache.log4j.Logger;

import com.sz.dao.UserDao;
import com.sz.dao.impl.UserDaoImpl;
import com.sz.pojo.User;
import com.sz.service.UserService;

public class UserServiceImpl implements UserService {
	// 声明Dao层对象
	UserDao userDao = new UserDaoImpl();
	// 声明lof4j对象
	Logger log = Logger.getLogger(this.getClass());

	// 用户登录
	@Override
	public User checkUserLoginService(String uname, String pwd) {
		return userDao.checkUserLoginDao(uname, pwd);
	}
	// 用户修改密码
	@Override
	public int userChangePwdSer(String newPwd, int uid) {
		log.debug(uid+":发起密码修改请求");
		int index = userDao.userChangePwdDao(newPwd, uid);
		if(index > 0) {
			log.debug(uid+":密码修改成功");
		}else {
			log.debug(uid+":密码修改失败");
		}
		return index;
	}
	// 查看所有用户信息
	@Override
	public List<User> userShowService(){
		return userDao.userShowDao();
	}
	// 用户注册
	@Override 
	public int userRegService(User u) {
		int index = userDao.userRegDao(u);
		if(index>0) {
			log.debug("用户："+u.getUname()+"，注册成功！");
		}else {
			log.debug("用户："+u.getUname()+"，注册失败！！");
		}
		return index;
	}
	

}
