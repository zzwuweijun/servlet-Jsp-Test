package com.io;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;

public class FileOutputStreamTest {
	@Test
	public void t3() {
		File file = new File("bb.txt");
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(file, true);
			byte[] b = new byte[] { 99, 98, 96, 99, 121, 111 };
			fos.write(b);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (fos != null) {
					fos.close();
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	// @Test
	// public void t2() {
	// File file = new File("bb.txt");
	// try {
	// FileOutputStream fos = new FileOutputStream(file, true);
	// fos.write(49);
	// fos.write('+');
	// fos.write(49);
	// fos.write('=');
	// fos.write(50);
	//
	// }catch(IOException e) {
	// e.printStackTrace();
	// }
	// }
	// @Test
	// public void t1() {
	// File file = new File("bb.txt");
	// try {
	// FileOutputStream fos = new FileOutputStream(file);
	// fos.write(49);
	// fos.flush();
	// System.out.println("..");
	// } catch (FileNotFoundException e) {
	// e.printStackTrace();
	// } catch (IOException e) {
	// // TODO Auto-generated catch block
	// e.printStackTrace();
	// }
	//
	// }
}
