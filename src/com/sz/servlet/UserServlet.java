package com.sz.servlet;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.sz.pojo.User;
import com.sz.service.UserService;
import com.sz.service.impl.UserServiceImpl;

public class UserServlet extends HttpServlet {
	Logger log = Logger.getLogger(UserService.class);
	// --获取service层对象
	UserService us = new UserServiceImpl();

	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 设置请求编码格式
		req.setCharacterEncoding("utf-8");
		// 设置响应编码格式
		resp.setContentType("text/html;charset=utf-8");
		// 获取请求数据
		String oper = req.getParameter("oper");
		if (oper.equals("login")) {
			// 调用登录处理方法
			checkUserLogin(req, resp);
		} else if (oper.equals("reg")) {
			// 调用注册处理方法
			userReg(req, resp);
		} else if (oper.equals("out")) {
			// 调用退出处理方法
			userOut(req, resp);
		} else if (oper.equals("pwd")) {
			// 调用修改密码处理方法
			userChangePwd(req, resp);
		} else if (oper.equals("showUser")) {
			// 调用查看所有用户信息处理方法
			userShow(req, resp);
		} else {
			System.out.println("错误。。。！");
		}

	}

	// 注册处理方法
	private void userReg(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		String uname = req.getParameter("uname");
		String pwd = req.getParameter("pwd");
		String sex = req.getParameter("sex");
		int age = req.getParameter("age") != "" ? Integer.parseInt(req.getParameter("age")) : 0;
		String birth = req.getParameter("birth") != "" ? req.getParameter("birth") : "1997-1-1";
		User u = new User(0, uname, pwd, sex, age, birth);
		int index = us.userRegService(u);
		if (index > 0) {// 成功
			resp.sendRedirect(req.getContextPath() + "/main/index.jsp");
		} else {// 失败
			req.getRequestDispatcher("/main/reg.jsp");
		}
	}

	// 查看所有用户信息
	private void userShow(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		List<User> lu = us.userShowService();
		log.debug("显示所有用户信息" + lu);
		if (lu != null) {
			req.setAttribute("lu", lu);
			req.getRequestDispatcher("/main/showUser.jsp").forward(req, resp);
			log.debug("显示所有用户成功");
		} else {
			req.getRequestDispatcher("/main/index2.jsp").forward(req, resp);
			log.debug("显示所有用户失败");
		}
	}

	// 用户修改密码
	private void userChangePwd(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		// 获取数据
		String newPwd = req.getParameter("newPwd");
		User u = (User) req.getSession().getAttribute("user");
		int uid = u.getId();
		// 处理请求，调用service处理
		int index = us.userChangePwdSer(newPwd, uid);
		String path = req.getContextPath();
		if (index > 0) {
			HttpSession hs = req.getSession();
			hs.setAttribute("pwd", 1);
			log.debug("请重新登录");
			resp.sendRedirect(path + "/main/index.jsp");
		} else {
			log.debug(uid + ":密码修改失败111");
		}
	}

	// 用户退出登录
	private void userOut(HttpServletRequest req, HttpServletResponse resp) {
		HttpSession hs = req.getSession();
		hs.invalidate();
		try {
			resp.sendRedirect(req.getContextPath() + "/main/index.jsp");
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	// 处理登录
	private void checkUserLogin(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// 获取请求信息
		String uname = req.getParameter("uname");
		String pwd = req.getParameter("pwd");
		log.debug(uname + ":" + pwd);
		// 处理请求信息
		// --获取service层对象，全局
		// --校验
		User u = us.checkUserLoginService(uname, pwd);
		log.debug(u);
		// --登录成功后将登录数据保存到session对象中
		String path = req.getContextPath();
		if (u != null) {
			HttpSession hs = req.getSession();
			hs.setAttribute("user", u);
			// 响应请求结果
			// --直接响应
			// --请求转发
			// --重定向
			log.debug("path---" + path);
			try {
				resp.sendRedirect(path + "/main/index2.jsp");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {// 登录失败
			// 添加标识符
			req.setAttribute("loginFlag", 0);
			// 请求转发 // 转发和重定向的路径问题查看文档。
			req.getRequestDispatcher("/main/index.jsp").forward(req, resp);

		}
	}

}
