package com.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.junit.Test;

public class FileInputStreamTest {
	@Test
	public void t2() {
		try {
			File file = new File("aa.txt");
			FileInputStream fis = new FileInputStream(file);
			byte[] b = new byte[5];
			int i = fis.read(b);
			while (i != -1) {
				String s = new String(b);
				System.out.println(s);
				i = fis.read(b);
			}
			
			
//			int i = fis.read();
//			while (i != -1) {
//				System.out.print((char)i);
//				i = fis.read();
//			}
			
//			while (i != -1) {
//				System.out.println(i);
//				i = fis.read();
//			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	} 
	
	
//	@Test
//	public void t1() {
//		File file = new File("tt\\bb");
//		file.delete();
//		file.mkdirs();
//		try {
//			file.createNewFile();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		System.out.println(file.listFiles());
//		System.out.println(file.listFiles().length);
//	}
}
