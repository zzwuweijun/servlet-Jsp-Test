package com.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.junit.Test;

public class OperateFile {
	@Test
	public void t1() {
//		t2(new File("aa.txt"), "tt");
		// File a = new File("aa.txt");
		// System.out.println(a.length());
		// long b = a.length();
		// int c = (int) b;
		// System.out.println(c);
		
		superCopeFile(new File("tt"), "bb//oo");
		
//		File f = new File("tt");
//		System.out.println(f.getName());
		//与系统相关的默认名称 - 分隔符字符，以方便的方式表示为字符串。 
//		System.out.println(File.separator);
	}
	/**
	 * 文件夹复制
	 * @param file 被复制的文件的File对象
	 * @param newPath 要保存到的位置的字符串
	 */
	public void superCopeFile(File file, String newPath) {
		String oldFilePath = file.getName();
		String newFilePath = newPath +File.separator+ oldFilePath;
		// 创建一个新的File对象
		File newFile = new File(newFilePath);
		// 获取当前传递进来的File对象所有子元素
		File[] files = file.listFiles();
		if (files != null) {// 说明当前传递进来的File对象不是文件
			newFile.mkdirs();
			System.out.println(newFile.getName() + "文件夹复制完毕！");
			// 里面的子元素
			if(files.length != 0) {
				for(File f:files) {
					this.superCopeFile(f, newFilePath);
				}
			} 
		} else {// 是一个文件则复制文件
			FileInputStream fis = null;
			FileOutputStream fos = null;
			try {
				fis = new FileInputStream(file);
				fos = new FileOutputStream(newFile);
				byte[] b = new byte[1024];
				int count = fis.read(b);
				while (count != -1) {
					fos.write(b);
					fos.flush();
					count = fis.read(b);
				}
				System.out.println(newFile.getName() + "复制完毕！");
			} catch (IOException e) {
				e.printStackTrace();
			} finally {// 关闭流
				if (fis != null) {
					try {
						fis.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
				if (fos != null) {
					try {
						fos.close();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
	}

	/**
	 * 文件复制
	 * 
	 * @param file 被复制的文件的File对象
	 * @param path 要保存到的位置的字符串
	 */
	public void t2(File file, String path) {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(file);
			File newFile = new File(path + "//" + file.getName());
			fos = new FileOutputStream(newFile);
			byte[] b = new byte[1024];
			int count = fis.read(b);
			while (count != -1) {
				// 可以在这里做一些操作，如加密
				// byte temp = b[0];
				// b[0] = b[1];
				// b[1] = temp;

				fos.write(b, 0, count);
				fos.flush();
				count = fis.read(b);
			}
			System.out.println("复制成功");

		} catch (IOException e) {
			e.printStackTrace();
		} finally {// 关闭流
			if (fis != null) {
				try {
					fis.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
